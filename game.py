# Aesthetic preference to delay messages displayed
from time import sleep

# Function defining entries on board
def print_board(entries):
    # Variables for board, new play on board, and how to restructure the board
    line = "+---+---+---+"
    output = line
    n = 0

    # Restructures board as moves are made, adding | depending on board number 1 - 9
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

# Function to delay Game over message... to add Suspense!
def suspense():
    sleep(0.5)
    print(".")
    sleep(0.5)
    print(".")
    sleep(0.5)
    print(".")
    sleep(0.5)
    print("Game Over")
    sleep(1.5)


# Create function to simplify code
# Two parameters winner and win message
# Accepts two arguments later: current_player and message 
# Display final board, display message and exit
def game_over(winner, win_message):
    print_board(board)
    print(winner, win_message)
    suspense()
    exit()

# Checks if top middle or bottom rows hold same value
def is_row_winner(board, row):
    row_winner = 0
    if board[0] == board[1] and board[1] == board[2]:
        row_winner = 1
        return True

    elif board[3] == board[4] and board[4] == board[5]:
        row_winner = 2
        return True

    elif board[6] == board[7] and board[7] == board[8]:
        row_winner = 3
        return True
    
def is_c_winner(board, column):
    c_winner = 0
    if board[0] == board[3] and board[3] == board[6]:
        c_winner = 1
        return True

    elif board[1] == board[4] and board[4] == board[7]:
        c_winner = 2
        return True

    elif board[2] == board[5] and board[5] == board[8]:
        c_winner = 3
        return True

def diagonal_win(board, diagonal):
    d_winner = 0
    if board[0] == board[4] and board[4] == board[8]:
        d_winner = 1
        return True
    elif board[2] == board[4] and board[4] == board[6]:
        d_winner = 2
        return True


# Variables for plays/board spaces and player
board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

# Function asking where players want to move
for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player
    message = "Has won"
    end_message = "Game Over"
    

    # If and elif checking for winner
    if is_row_winner(board, 1):
        game_over(current_player, message)

    elif is_row_winner(board, 2):
        game_over(current_player, message)

    elif is_row_winner(board, 3):
        game_over(current_player, message)

    elif is_c_winner(board, 1):
        game_over(current_player, message)

    elif is_c_winner(board, 2):
        game_over(current_player, message)

    elif is_c_winner(board, 3):
        game_over(current_player, message)

    elif diagonal_win(board, 1):
        game_over(current_player, message)

    elif diagonal_win(board, 2):
        game_over(current_player, message)

    # Switches player every other time
    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
sleep(1.5)
